<?php
include_once 'Alumno.php';
include_once 'Curso.php';
/**
 * Description of Files
 *
 * @author Raquel Pont <raquel.pont.lillo@gmail.com>
 */
class Files {
    private $falumnos = "../media/database/alumnos.csv";
    private $fcursos = "../media/database/cursos.csv";
    
    
    public function createAlumno($alumno) {
	$f = fopen($this->falumnos, "a");
	$linea = $alumno->__GET('id') . ";"
		. $alumno->__GET('nombre') . ";"
		. $alumno->__GET('curso')->__GET('id') . "\r\n";
	fwrite($f, $linea);
	fclose($f);
    }

    public function readAlumnos() {
	$alumnos = array();
	
	if ($archivo = fopen($this->falumnos, "r")) {
	    $token = fgetcsv($archivo, 0, ";");
	    while ($token) {
		$alumno = new Alumno ($token[0], $token[1], $token[2]);
		array_push($alumnos, $alumno);
		$token = fgetcsv($archivo, 0, ";");
	    }
	    fclose($archivo);
	} else {
	    //errores
	}
        
	return $alumnos;
    }

    public function createCurso($curso) {
	$fc = fopen($this->fcursos, "a");
	$linea = $curso->__GET('id') . ";"
		. $curso->__GET('nombre') . ";"
		. $curso->__GET('horas') . "\r\n";
	fwrite($fc, $linea);
	fclose($fc);
    }

    public function readCursos() {
	$cursos = array();
	
	if ($archivoc = fopen($this->fcursos, "r")) {
	    $tokenc = fgetcsv($archivoc, 0, ";");
	    while ($tokenc) {
		$curso = new Curso ($tokenc[0], $tokenc[1], $tokenc[2]);
		array_push($cursos, $curso);
		$tokenc = fgetcsv($archivoc, 0, ";");
	    }
	    fclose($archivoc);
	} else {
	    
	}

	return $cursos;
    }
}
