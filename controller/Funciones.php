<?php

function recoge($valor) {
    if (isset($_REQUEST[$valor])) {
        $resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
    } else {
        $resultado = "";
    }
    return $resultado;
}
