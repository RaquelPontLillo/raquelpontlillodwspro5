<?php

include_once '../model/Files.php';
include_once '../model/Alumno.php';
include_once '../model/Curso.php';
include_once 'Funciones.php';

function listar() {
    $alumnos = new Files();
    foreach ($alumnos->readAlumnos() as $alumno) {
        echo "<tr>
                <td>" . $alumno->__GET('id') . "</td>
                <td>" . $alumno->__GET('nombre') . "</td>
    		<td>" . $alumno->__GET('curso') . "</td>
            </tr>";
    }
}

function cargaCombo() {
    $cursos = new Files();
    foreach ($cursos->readCursos() as $curso) {
        echo "<option value='" . $curso->__GET('id') . "'>" . $curso->__GET('nombre') . " (" . $curso->__GET('horas') . "h)</option>";
    }
}

function calcularID() {
    $modelo = new Files();
    $alumnos = $modelo->readAlumnos();
    $ultAlumno = end($alumnos);
    $ultID = $ultAlumno->__GET('id');
    $ultID++;
    echo $ultID;
}

function guardar() {
    $id = recoge('id');
    $nombre = recoge('nombre');
    $idcurso = recoge('curso');

    if ($id == "" || $nombre == "" || $idcurso == 0) {
        echo "<p style='color:red'>Alguno de los campos está vacío. Rellénelos primero.</p>"
        . "<p><a href='../index.php'>Volver a inicio</a> || <a href='../view/VistaCurso.php'>Volver a gestión de cursos</a></p>";
    } else {
        $curso = new Curso($idcurso, null, null);
        $alumno = new Alumno($id, $nombre, $curso);
        $modelo = new Files();
        $modelo->createAlumno($alumno);
        echo "<p style='color:blue'>Los datos se han guardado con éxito.</p>"
        . "<p><a href='../index.php'>Volver a inicio</a> || <a href='../view/VistaAlumno.php'>Volver a gestión de Alumnos</a></p>";
    }
}
