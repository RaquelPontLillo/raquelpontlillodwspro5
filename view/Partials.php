<?php
include_once 'Config.php';

function myheader() {
    echo "<h1>" . Config::$titulo . "</h1><hr/>\n";
}

function myfooter() {
    echo "<hr/><pre>" . Config::$empresa . " " . Config::$autor . " ";
    echo Config::$anio . " " . Config::$fecha . "</pre>\n";
}

function mymenu() {
    echo "<ul>
            <li><a href='../index.php'>Inicio</a></li>
            <li><a href='VistaCurso.php'>Gestión de cursos</a></li>
            <li><a href='VistaAlumno.php'>Gestión de alumnos</a></li>
            <li><a href='../media/docs/documentacion.pdf'>Documentación</a></li>
        </ul>";
}

function mymenuindex() {
    echo "<ul>
            <li><a href='index.php'>Inicio</a></li>
            <li><a href='view/VistaCurso.php'>Gestión de cursos</a></li>
            <li><a href='view/VistaAlumno.php'>Gestión de alumnos</a></li>
            <li><a href='media/docs/documentacion.pdf'>Documentación</a></li>
        </ul>";
}