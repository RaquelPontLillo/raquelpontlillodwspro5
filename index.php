<!DOCTYPE html>

<html>
    <head>
        <title>Vista general | Matrículas App. 2016-2017</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="media/css/pure-min.css">
        <style>
            img{ height: 150px; }
            ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: #333;
            }

            li {
                float: left;
            }

            li a {
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            li a:hover {
                background-color: #111;
            }
        </style>
    </head>
    <body>
        <?php include_once 'view/Partials.php';
        mymenuindex();
        myheader();
        ?>
        <div class="pure-g">
            <h1>¿Qué quieres hacer?</h1>
            <div class="pure-u-1-12">
                <table>
                    <tr>
                        <th>Gestionar cursos</th>
                        <td><a href="view/VistaCurso.php"><img src ="media/images/curso.jpg"/></a></td>
                    </tr>
                    <tr>
                        <th>Gestionar alumnos</th>
                        <td><a href="view/VistaAlumno.php"><img src ="media/images/alumno.jpg"/></a></td>
                    </tr>
                    <tr>
                        <th>Ver documentación</th>
                        <td><a href="media/docs/documentacion.pdf"><img src ="media/images/documentacion.jpg"/></a></td>
                    </tr>
                </table>
            </div>
        </div>
        <?php myfooter(); ?>
    </body>
</html>